# Find duplicate files
Search and create links when files are duplicated in the specified folder.

This is the first program I wrote.
Simply specify the path to a folder, and the program will search for and replace/rename (depending on the options chosen) the files that are duplicated.
It does not compare files by name but by checksum.

The python libraries (python2.7) that must be installed are: bone, stat, hashlib, sample, virtual_memory, Tkinter, tkMessageBox, tkFileDialog, PIL (pip install pillow), sys.


## Usage

### Installation
```bash
git clone https://gitlab.com/Breizhux/find_duplicate_files
cd find_duplicate_files/
virtualenv ./ && source bin/activate
pip3 install tk psutil pillow
```

### Execution
```bash
#go to find_duplicate_files directory :
cd find_duplicate_files/
#run :
source bin/activate
python3 recherche_de_fichier_en_double.py
```
